# CVS to Git with Reposurgeon

## Requirements (apt install these)

```
cvs
reposurgeon
git
```

## Example CVS Repo

http://dev-cpp.cvs.sourceforge.net/

## Steps to convert from CVS to Git

```bash
mkdir scratch-pad
cd scratch-pad
repotool initialize dev-cpp
# source is cvs
# destination is git
vim Makefile
# walk through instructions at top of Makefile
make stubfile
# in real application, update .map file accordingly
make
cd dev-cpp-git
git remote add origin <remote-git-repo>
git push origin master
```

[Reposurgeon process docs](http://www.catb.org/~esr/reposurgeon/repository-editing.html)

